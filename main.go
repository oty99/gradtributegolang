package main

import (
	"net/http"
	"os"
	"strconv"
	"gopkg.in/mgo.v2"
	"net"
	"crypto/x509"
	"crypto/tls"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/ini.v1"
	"encoding/base64"

	"github.com/gorilla/securecookie"

	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/oty99/gradtributegolang/routes"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/utilities"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/websessions"
)



// Main entry point for this web service container
func main() {
	settingsFolder := "config"
	settingsFile := "settings.ini"
	utilities.Info("Initializing Web Service.. Reading Database Settings")

	isDir, _ := utilities.FileExists(settingsFolder)
	if !isDir {
		utilities.Warn("Failed to find configuration folder, creating one in the application path..")
		os.Mkdir(settingsFolder, 0777)
	}
	settingsLocation := settingsFile
	if len(settingsFolder) > 0 {
		settingsLocation = settingsFolder + string(os.PathSeparator) + settingsLocation
	}
	dbSettings := &globals.DatabaseSettings{Host:"127.0.0.1",Port:27017,DatabaseName:"grad_tribute",CollectionPrefix:"dev1_",Username:"",Password:"",UseSSL:false,RootPEM:""}
	isFile, _ := utilities.FileExists(settingsLocation)
	cfg := ini.Empty()
	if !isFile {
		utilities.Warn("Failed to find settings file, creating one with defaults..")
		dbSection, _ := cfg.NewSection("Database")
		dbSection.NewKey("Host", dbSettings.Host)
		dbSection.NewKey("Port", strconv.Itoa(dbSettings.Port))
		dbSection.NewKey("DatabaseName", dbSettings.DatabaseName)
		dbSection.NewKey("CollectionPrefix", dbSettings.CollectionPrefix)
		dbSection.NewKey("Username", dbSettings.Username)
		dbSection.NewKey("Password", dbSettings.Password)
		dbSection.NewKey("UseSSL", strconv.FormatBool(dbSettings.UseSSL))

		err := cfg.SaveTo(settingsLocation)
		if err != nil {
			utilities.FErrorf("Failed to write the default setting file with error:%s", err.Error())
		}
	}else {
		cfg, err := ini.Load(settingsLocation)
		if err != nil {
			utilities.FErrorf("Failed to load the setting file with error:%s", err.Error())
		}
		dbSection, err := cfg.GetSection("Database")
		if err != nil {
			utilities.FErrorf("Failed to load the setting file Database section with error:%s", err.Error())
		}
		dbSection.Key("Host").MustString(dbSettings.Host)
		dbSection.Key("Port").MustInt(dbSettings.Port)
		dbSection.Key("DatabaseName").MustString(dbSettings.DatabaseName)
		dbSection.Key("CollectionPrefix").MustString(dbSettings.CollectionPrefix)
		dbSection.Key("Username").MustString(dbSettings.Username)
		dbSection.Key("Password").MustString(dbSettings.Password)
		dbSection.Key("UseSSL").MustBool(false)
		dbSection.Key("RootPEM").MustString(dbSettings.RootPEM)

		dbSettings.Host = dbSection.Key("Host").String()
		dbSettings.Port, err = dbSection.Key("Port").Int()
		if err != nil {
			utilities.FErrorf("Failed to load the setting file Database section with error:%s", err.Error())
		}
		dbSettings.DatabaseName = dbSection.Key("DatabaseName").String()
		dbSettings.CollectionPrefix = dbSection.Key("CollectionPrefix").String()
		dbSettings.Username = dbSection.Key("Username").String()
		dbSettings.Password = dbSection.Key("Password").String()
		dbSettings.UseSSL, err = dbSection.Key("UseSSL").Bool()
		if err != nil {
			utilities.FErrorf("Failed to load the setting file Database section with error:%s", err.Error())
		}
		dbSettings.RootPEM = dbSection.Key("RootPEM").String()
	}

	globals.SettingFileLocation = settingsLocation
	utilities.Infof("Settings loaded : [%s]", globals.SettingFileLocation)
	utilities.Infof("Connecting to database: [%s] on [%s:%d]", dbSettings.DatabaseName, dbSettings.Host, dbSettings.Port)

	var dbSession *mgo.Session
	var err error
	if dbSettings.UseSSL {
		if dbSettings.RootPEM == "" {
			utilities.FError("Invalid root PEM for a use SSL databse connection")
		}

		var dial func(addr net.Addr) (net.Conn, error)
		dial = func(addr net.Addr) (net.Conn, error) {
			// First, create the set of root certificates. For this example we only
			// have one. It's also possible to omit this in order to use the
			// default root set of the current operating system.
			roots := x509.NewCertPool()
			operationOk := roots.AppendCertsFromPEM([]byte(dbSettings.RootPEM))
			if !operationOk {
				utilities.PError("Failed to parse root certificate")
			}
			conn, err := tls.Dial("tcp", addr.String(), &tls.Config{RootCAs: roots})
			if err != nil {
				utilities.Errorf("Error occured while connection to the database : %v", err)
				return nil, err
			}
			return conn, nil
		}

		dInfo := &mgo.DialInfo{Addrs: []string{dbSettings.Host + ":" + strconv.Itoa(dbSettings.Port)}, Dial: dial}
		dbSession, err = mgo.DialWithInfo(dInfo)
	} else {
		dbSession, err = mgo.Dial(dbSettings.Host + ":" + strconv.Itoa(dbSettings.Port))
	}
	if err != nil {
		utilities.PErrorf("Error occured while connection to the database : %v", err)
	}
	defer dbSession.Close()
	globals.DBSession = dbSession
	globals.DBSettings = dbSettings

	dbHandle := globals.DBSession.DB(globals.DBSettings.DatabaseName)

	if globals.DBSettings.Username != "" {
		err := dbHandle.Login(globals.DBSettings.Username, globals.DBSettings.Password)
		if err != nil {
			utilities.PErrorf("Failed to authenticate the database with Username [%s] and Password [%s] : %v", globals.DBSettings.Username, globals.DBSettings.Password, err)
		}
	}

	dbCollHandle := dbHandle.C(globals.DBSettings.CollectionPrefix + "configuration")
	pQuery := dbCollHandle.Find(bson.M{"Type": "Startup"})
	iConfigs, err := pQuery.Count()
	if err != nil {
		utilities.PErrorf("Error occured while trying to query for configuration : %v", err)
	}
	var result struct {
		ID                     bson.ObjectId   `bson:"_id,omitempty"`
		ConfigType             string `bson:"Type"`
		WebSrcPath             string `bson:"WebHostPath"`
		ApplicationBindHost    string `bson:"BindHost"`
		ApplicationHostName    string `bson:"HostName"`
		SessionTimeout         int64  `bson:"SessionTimeout"`
		ApplicationHostPort    int    `bson:"HostPort"`
		IsProduction           bool   `bson:"IsProduction"`
		SecureCookieAuthKey    string `bson:"CookieAuthKey"`
		SecureCookieEncryptKey string `bson:"CookieEncryptKey"`
		WebPath                string `bson:"WebPath"`
	}
	if iConfigs > 0 {

		err = pQuery.One(&result)
		if err != nil {
			utilities.PErrorf("Failed while querying the database for startup config : %v", err)
		}
		globals.WebSrcPath = result.WebSrcPath
		globals.ApplicationHostName = result.ApplicationHostName
		globals.SessionTimeout = result.SessionTimeout
		globals.ApplicationHostPort = result.ApplicationHostPort
		globals.IsProduction = result.IsProduction
		globals.ApplicationBindHost = result.ApplicationBindHost
		globals.SecureCookieAuthKey = result.SecureCookieAuthKey
		globals.SecureCookieEncryptKey = result.SecureCookieEncryptKey
		globals.WebPath = result.WebPath
		globals.BaseAlerts = make(map[string]globals.BaseAlertInfo);

	} else {
		utilities.Warn("Failed to load startup config. no startup found. Would insert default config")
		globals.WebSrcPath = "~/go/web"
		globals.ApplicationHostName = "localhost"
		globals.SessionTimeout = -1
		globals.ApplicationHostPort = 5000
		globals.IsProduction = false
		globals.ApplicationBindHost = ""
		globals.SecureCookieAuthKey = ""
		globals.SecureCookieEncryptKey = ""
		globals.WebPath = "~/go/web"
		globals.BaseAlerts = make(map[string]globals.BaseAlertInfo);

		result.ConfigType = "Startup"
		result.WebSrcPath = globals.WebSrcPath
		result.ApplicationHostName = globals.ApplicationHostName
		result.SessionTimeout = globals.SessionTimeout
		result.ApplicationHostPort = globals.ApplicationHostPort
		result.IsProduction = globals.IsProduction
		result.ApplicationBindHost = globals.ApplicationBindHost
		result.SecureCookieAuthKey = globals.SecureCookieAuthKey
		result.SecureCookieEncryptKey = globals.SecureCookieEncryptKey
		result.WebPath = globals.WebPath

		err = dbCollHandle.Insert(result)
		if err != nil {
			utilities.PErrorf("Failed while inserting default startup configuration : %v", err)
		}
		pQuery = dbCollHandle.Find(bson.M{"Type": "Startup"})
		iConfigs, err = pQuery.Count()
		if err != nil {
			utilities.PErrorf("Error occured while trying to query for configuration : %v", err)
		}
		if iConfigs > 0 {
			err = pQuery.One(&result)
		}
		if ((err != nil) || (iConfigs == 0)) {
			utilities.PErrorf("Failed while querying the database for startup config : %v", err)
		}
	}
	// Validate the web host path
	isDir, err = utilities.FileExists(globals.WebPath)
	if (!isDir) || (err != nil) {
		utilities.PErrorf("Web host path specified : [%s] is invalid", globals.WebPath)
	}
	globals.DBSession.SetMode(mgo.Monotonic, true)
	globals.WebCookiesStore = nil
	globals.SessionsStore = nil

	if (globals.SecureCookieAuthKey == ""){
		cookieAuthKey := securecookie.GenerateRandomKey(64);
		cookieEncKey := securecookie.GenerateRandomKey(32);
		if ((cookieAuthKey == nil) || (cookieEncKey == nil)){
			utilities.PErrorf("Failed generate one time random key for cookie ans session encryption : %v", err)
		}
		globals.SecureCookieAuthKey = base64.StdEncoding.EncodeToString(cookieAuthKey)
		globals.SecureCookieEncryptKey = base64.StdEncoding.EncodeToString(cookieEncKey)

		pQuery = dbCollHandle.Find(bson.M{"Type": "Startup"})
		err = dbCollHandle.UpdateId(result.ID, bson.M{"$set":bson.M{"CookieAuthKey":globals.SecureCookieAuthKey, "CookieEncryptKey":globals.SecureCookieEncryptKey}})
		if err != nil {
			utilities.PErrorf("Failed set one time Cookie Auth and Encrypt keys into the DB : %v", err)
		}
	}

	utilities.Info("Initializing Secure Cookies..")
	secureCookieAuth, err := base64.StdEncoding.DecodeString(globals.SecureCookieAuthKey)
	if err != nil {
		utilities.PErrorf("Failed decoding secure authentication cookie string: %v", err)
	}
	secureCookieEncrypt, err := base64.StdEncoding.DecodeString(globals.SecureCookieEncryptKey)
	if err != nil {
		utilities.PErrorf("Failed decoding secure encryption cookie string: %v", err)
	}
	globals.WebCookiesStore = websessions.NewCookieStore(secureCookieAuth, secureCookieEncrypt)
	globals.SessionsStore = websessions.NewDatabaseStore(globals.DBSession, globals.DBSettings.DatabaseName, globals.DBSettings.CollectionPrefix, secureCookieAuth, secureCookieEncrypt)

	utilities.Infof("Changing working directory to Web Path : %s", globals.WebPath)
	err = os.Chdir(globals.WebPath)
	if err != nil {
		utilities.PErrorf("Failed to set the Web Path for the Application : %v", err);
	}

	rootRouter := routes.Initialize();

	http.Handle("/", rootRouter)
	http.ListenAndServe(globals.ApplicationBindHost+":"+strconv.Itoa(globals.ApplicationHostPort), nil) // Use the DefaultServerMux


}
