package user

import (
	"gopkg.in/mgo.v2/bson"
	"bitbucket.org/implasmicjafar/enjaeger/models/login"
)

type UserStruct struct {
	Id                  bson.ObjectId   `bson:"_id,omitempty"`
	UserName            string   `bson:"username" json:"username"`
	FirstName           string   `bson:"firstname" json:"firstname"`
	LastName            string   `bson:"lastname" json:"lastname"`
	MiddleName	    string   `bson:"middlename" json:"middlename"`
	Email               string   `bson:"email" json:"email"`
	Role                string   `bson:"role" json:"role"`
	PositionType            string   `bson:"positiontype" json:"positiontype"`
	Phone               login.PhoneStructure   `bson:"phone" json:"phone"`
	Address             login.AddressStructure  `bson:"address" json:"address"`
	UserType		string `bson:"usertype" json:"usertype"`
	Age 			int	`bson:"age" json:"age"`
	Gender 			string `bson:"gender" json:"gender"`
	Position 		string `bson:"position" json:"position"`
	Avatar			string `bson:"avatar" json:"avatar"`
	SessionId           string   `bson:"sessionid"`
	Password            string   `bson:"passwordhash" json:"password"`
	LastLogon           string   `bson:"lastlogon" json:"lastlogon"`
	RecentLogon         string   `bson:"recentlogon" json:"recentlogon"`
}