package misc

import (
	"bitbucket.org/oty99/gradtributegolang/models/user"
)

type LoginData struct {
	Email		string `json:"email"`
	Valid 		bool `json:"valid"`
	Profile         user.UserStruct `json:"profile"`
	Error           string `json:"error"`
	ErrorDetails	string `json:"errorDetail"`
	Token           string `json:"token"`
	SessionId       string `json:"session_id"`

}

type LogoutData struct {
	Email		string `json:"email"`
	Valid 		bool `json:"valid"`
	LoggedOut 	bool `json:"loggedout"`
}

