package misc

type SignupData struct{
	Email		string `json:"email"`
	Valid 		bool `json:"valid"`
	Error           string `json:"error"`
	ErrorDetails	string `json:"errorDetail"`
	Token           string `json:"token"`
	SessionId       string `json:"session_id"`
	Message 	string `json:"message"`
}