package generic

import (
	"time"

	"bitbucket.org/oty99/gradtributegolang/models/misc"
	"gopkg.in/mgo.v2/bson"
)

type QuizStruct struct {
	Question    string   `bson:"question" json:"question"`
	Options     []string `bson:"options" json:"options"`
	AnswerIndex int      `bson:"answerindex" json:"answerindex"`
}

type ModuleInfo struct {
	Id     bson.ObjectId `bson:"_id,omitempty"`
	Stages []InfoStage   `bson:"stages" json:"stages"`
	Module string        `bson:"module" json:"module"`
}

type InfoStage struct {
	Title    string       `bson:"title" json:"title"`
	Text     string       `bson:"text" json:"text"`
	MoreText string       `bson:"moretext" json:"moretext"`
	Quiz     []QuizStruct `bson:"quiz" json:"quiz"`
}

type ModuleStruct struct {
	UserDetails ModuleUserScore `json:"userdetails"`
	Info        ModuleInfo      `json:"info"`
	Error       string          `json:"error"`
	LoginData   misc.LoginData  `json:"logindata"`
}

type ModuleUserScore struct {
	Id     bson.ObjectId      `bson:"_id,omitempty"`
	UserId bson.ObjectId      `bson:"userid" json:"userid"`
	Stages []ModuleScoreStage `bson:"stages" json:"stages"`
}

type ModuleScoreStage struct {
	Stage        int       `bson:"stage" json:"stage"`
	Name         string    `bson:"name" json:"name"`
	Enabled      bool      `bson:"enabled" json:"enabled"`
	Completed    bool      `bson:"completed" json:"completed"`
	CompleteTime time.Time `bson:"completetime" json:"completetime"`
	Score        float32   `bson:"score" json:"score"`
}
