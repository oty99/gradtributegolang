package profile

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/middleware"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/utilities"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"gopkg.in/mgo.v2/bson"

	"errors"
	"io/ioutil"
	"mime/multipart"
	"os"
	"path/filepath"

	"encoding/base32"

	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/oty99/gradtributegolang/controllers/base"
	"bitbucket.org/oty99/gradtributegolang/models/misc"
	"bitbucket.org/oty99/gradtributegolang/models/user"
)

// Initialize the routes that could come to this controller
// Generally, we expect that javascript jQuery posts would be used for
// submissions. However we also expect a get request for the login page
func Initialize(rRouter *mux.Router) {
	// The routes passed in here are built upon
	lChain := alice.New(middleware.GZIPHandler, middleware.SessionHandler)
	rRouter.Handle("/profile", lChain.ThenFunc(handleProfileUpdate))
}

// Accept to upload and update profile
func handleProfileUpdate(w http.ResponseWriter, r *http.Request) {
	// First verify that the user is logged in
	var lastUsername string = ""
	isLoggedIn, _ := middleware.IsLoggedIn(r)
	if isLoggedIn {
		loginData := misc.LoginData{Error: "", Email: "", ErrorDetails: "", Token: "", SessionId: ""}
		if userCookie, err := globals.WebCookiesStore.Get(r, "login-info"); err == nil {
			if userCookie.Values["username"] != nil {
				lastUsername = userCookie.Values["username"].(string)
			}
		}
		loginData.Email = lastUsername
		if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
			if (authCookie.Values["lastloginattempt"] != nil) && (authCookie.Values["lastloginattempt"] != "") {
				currToken := string(authCookie.Values["token"].(string))
				rsessid, _ := utilities.GetSessionID(r)
				loginData.Token = currToken
				loginData.SessionId = rsessid
			}
		}
		if (loginData.Email != "") && (loginData.Token != "") && (loginData.SessionId != "") {
			dbColHandler := globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "users")
			pQuery := dbColHandler.Find(bson.M{"email": loginData.Email})
			iCount, err := pQuery.Count()
			if err == nil {
				if iCount > 0 {
					auser := user.UserStruct{}
					err = pQuery.One(&auser)
					if err == nil {
						err = dbColHandler.UpdateId(auser.Id, bson.M{"$set": bson.M{"sessionid": loginData.SessionId}})
						if err == nil {
							const _24K = (1 << 20) * 1024
							if err := r.ParseMultipartForm(_24K); nil != err {
								err = r.ParseForm()
								if err != nil {
									base.ErrorHandler(w, r, err, http.StatusInternalServerError)
									return
								}
							}
							info := r.FormValue("info")
							if len(info) == 0 {
								base.ErrorHandler(w, r, errors.New("Invalid input parameters"), http.StatusInternalServerError)
								return
							}
							var imgFile string = ""
							{
								var infile multipart.File
								var targetFile *os.File
								var header *multipart.FileHeader
								infile, header, err = r.FormFile("file")
								if err == nil {
									defer infile.Close()
									targetFile, err = ioutil.TempFile(globals.WebPath+string(os.PathSeparator)+"assets"+string(os.PathSeparator)+"avatars", filepath.Ext(header.Filename))
									if err == nil {
										var written int64
										written, err = io.Copy(targetFile, infile)
										if (err == nil) && (written > 0) {
											imgFile = targetFile.Name()
											imgFile = "assets/avatars/" + filepath.Base(imgFile)
										} else {
											targetFile.Close()
										}
									}
								}
							}
							profile := user.UserStruct{}
							err = json.Unmarshal([]byte(string(info)), &profile)
							if err != nil {
								base.ErrorHandler(w, r, err, http.StatusInternalServerError)
								return
							}
							if len(imgFile) > 0 {
								profile.Avatar = imgFile
							}
							profile.SessionId = loginData.SessionId
							err = dbColHandler.UpdateId(auser.Id, &profile)
							if err != nil {
								base.ErrorHandler(w, r, err, http.StatusInternalServerError)
								return
							}
							if len(profile.Password) > 0 {
								dbPassword := base32.StdEncoding.EncodeToString([]byte(profile.Password))
								profile.Password = dbPassword
							} else {
								profile.Password = auser.Password
							}
							if len(imgFile) > 0 {
								profile.Avatar = imgFile
							}
							profile.SessionId = loginData.SessionId
							err = dbColHandler.UpdateId(auser.Id, &profile)
							if err != nil {
								base.ErrorHandler(w, r, err, http.StatusInternalServerError)
								return
							}

							if err == nil {
								sess, _ := utilities.GetSession(r)
								sess.Values["loggedin"] = true
								sess.Save(r, w)

								// We've set everything necessary
								loginData.Valid = true
								loginData.Profile = profile
								w.Header().Set("Content-type", "application/json")
								json_bytes, _ := json.Marshal(loginData)
								fmt.Fprintf(w, "%s\n", json_bytes)
								return
							}
						}
					}
				}
			}
		}
	}
	base.DefaultOutput(w,r,lastUsername);
}
