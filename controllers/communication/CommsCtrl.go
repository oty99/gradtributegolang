package communication

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/justinas/alice"

	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/middleware"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/utilities"
	"bitbucket.org/oty99/gradtributegolang/controllers/base"
	"bitbucket.org/oty99/gradtributegolang/controllers/generic"
	"bitbucket.org/oty99/gradtributegolang/models/misc"
)

// Initialize the routes that could come to this controller
// Generally, we expect that javascript jQuery posts would be used for
// submissions. However we also expect a get request for the login page
func Initialize(rRouter *mux.Router) {
	// The routes passed in here are built upon
	lChain := alice.New(middleware.GZIPHandler, middleware.SessionHandler)
	rRouter.Handle("/communication", lChain.ThenFunc(handleLoadComms)).Methods("POST")
}

// Load  everything needed for the user as well as communication utilities
func handleLoadComms(w http.ResponseWriter, r *http.Request) {
	var lastUsername string = ""
	isLoggedIn, _ := middleware.IsLoggedIn(r)
	if isLoggedIn {
		loginData := misc.LoginData{Error: "", Email: "", ErrorDetails: "", Token: "", SessionId: ""}
		if userCookie, err := globals.WebCookiesStore.Get(r, "login-info"); err == nil {
			if userCookie.Values["username"] != nil {
				lastUsername = userCookie.Values["username"].(string)
			}
		}
		currToken := ""
		rsessid := ""
		loginData.Email = lastUsername
		if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
			if (authCookie.Values["lastloginattempt"] != nil) && (authCookie.Values["lastloginattempt"] != "") {
				currToken = string(authCookie.Values["token"].(string))
				rsessid, _ = utilities.GetSessionID(r)
				loginData.Token = currToken
				loginData.SessionId = rsessid
			}
		}
		if (loginData.Email != "") && (loginData.Token != "") && (loginData.SessionId != "") {
			moduleReport, err := generic.GetModuleOutput("Communication", loginData)
			if err != nil {
				base.ErrorHandler(w, r, err, http.StatusInternalServerError)
				return
			}

			w.Header().Set("Content-type", "application/json")
			json_bytes, _ := json.Marshal(moduleReport)
			fmt.Fprintf(w, "%s\n", json_bytes)
			return
		}
	}
	base.DefaultOutput(w, r, lastUsername)
}
