package base

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"
	"crypto/md5"
	"io"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/utilities"
	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/oty99/gradtributegolang/models/misc"
)

type ErrorHandlerMsg struct {
	Error   string `json:"error"`
	Details string `json:"errorDetail"`
}

// Handler to take care of requests made for pages or routes that do not exist.
// Note that even this passes through the sefault session handler so, expect that most logging
// have already been accomplished
func Error404Handler(w http.ResponseWriter, r *http.Request) {
	errorMsg := ErrorHandlerMsg{Error: "Error404 Cannot find specified route", Details: "The path " + r.URL.RawPath + " was not found"}
	w.Header().Set("Content-type", "application/json")
	json_bytes, _ := json.Marshal(errorMsg)
	fmt.Fprintf(w, "%s\n", json_bytes)
}

// Handle generic errors from http
func ErrorHandler(w http.ResponseWriter, r *http.Request, err error, errorType int) {
	errorMsg := ErrorHandlerMsg{Error: err.Error(), Details: "An Error of Type " + strconv.Itoa(errorType) + "  has occured.\n" + err.Error()}
	w.Header().Set("Content-type", "application/json")
	json_bytes, _ := json.Marshal(errorMsg)
	fmt.Fprintf(w, "%s\n", json_bytes)
}

// What to push out when authentication could not be confirmed
func DefaultOutput(w http.ResponseWriter, r *http.Request, lastUsername string) {
	crutime := time.Now().Unix()
	h := md5.New()
	io.WriteString(h, strconv.FormatInt(crutime, 10))
	token := fmt.Sprintf("%x", h.Sum(nil))

	loginData := &misc.LoginData{ErrorDetails: ""}
	loginData.Error = ""
	loginData.Email = lastUsername
	loginData.SessionId, _ = utilities.GetSessionID(r)
	loginData.Token = token
	loginData.Valid = false
	if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
		authCookie.Options.Path = "/"
		authCookie.Values["lastloginattempt"], err = time.Now().UTC().MarshalText()
		authCookie.Values["token"] = token
		authCookie.Save(r, w)
	}
	sess, _ := utilities.GetSession(r)
	sess.Values["loginmsg"] = nil
	sess.Save(r, w)

	w.Header().Set("Content-type", "application/json")
	json_bytes, _ := json.Marshal(loginData)
	fmt.Fprintf(w, "%s\n", json_bytes)
}
