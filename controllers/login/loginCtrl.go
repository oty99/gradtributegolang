package login

import (
	"crypto/md5"
	"encoding/base32"
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/implasmicjafar/enjaeger/libraries/middleware"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/utilities"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"gopkg.in/mgo.v2/bson"

	"encoding/json"

	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/oty99/gradtributegolang/controllers/base"
	"bitbucket.org/oty99/gradtributegolang/models/misc"
	"bitbucket.org/oty99/gradtributegolang/models/user"
)

// Initialize the routes that could come to this controller
// Generally, we expect that javascript jQuery posts would be used for
// submissions. However we also expect a get request for the login page
func Initialize(rRouter *mux.Router) {
	// The routes passed in here are built upon
	lChain := alice.New(middleware.GZIPHandler, middleware.SessionHandler)
	rRouter.Handle("/login", lChain.ThenFunc(handleLogin)).Methods("POST")
	rRouter.Handle("/login", lChain.ThenFunc(handleSetup)).Methods("GET")
	rRouter.Handle("/logout", lChain.ThenFunc(handleLogout))
}

// Setup session and Cookie
func handleSetup(w http.ResponseWriter, r *http.Request) {
	var lastUsername string = ""
	isLoggedIn, _ := middleware.IsLoggedIn(r)
	if isLoggedIn {
		loginData := misc.LoginData{Error: "", Email: "", ErrorDetails: "", Token: "", SessionId: ""}
		if userCookie, err := globals.WebCookiesStore.Get(r, "login-info"); err == nil {
			if userCookie.Values["username"] != nil {
				lastUsername = userCookie.Values["username"].(string)
			}
		}
		loginData.Email = lastUsername
		if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
			if (authCookie.Values["lastloginattempt"] != nil) && (authCookie.Values["lastloginattempt"] != "") {
				currToken := string(authCookie.Values["token"].(string))
				rsessid, _ := utilities.GetSessionID(r)
				loginData.Token = currToken
				loginData.SessionId = rsessid
			}
		}
		if (loginData.Email != "") && (loginData.Token != "") && (loginData.SessionId != "") {
			dbColHandler := globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "users")
			pQuery := dbColHandler.Find(bson.M{"email": loginData.Email})
			iCount, err := pQuery.Count()
			if err == nil {
				if iCount > 0 {
					user := user.UserStruct{}
					err = pQuery.One(&user)
					if err == nil {
						err = dbColHandler.UpdateId(user.Id, bson.M{"$set": bson.M{"sessionid": loginData.SessionId}})
						if err == nil {
							err = logUserLogin(r, &user)
							if err == nil {
								sess, _ := utilities.GetSession(r)
								sess.Values["loggedin"] = true
								sess.Save(r, w)

								// We've set everything necessary
								loginData.Valid = true
								loginData.Profile = user
								w.Header().Set("Content-type", "application/json")
								json_bytes, _ := json.Marshal(loginData)
								fmt.Fprintf(w, "%s\n", json_bytes)
								return
							}
						}
					}
				}
			}
		}
	}

	crutime := time.Now().Unix()
	h := md5.New()
	io.WriteString(h, strconv.FormatInt(crutime, 10))
	token := fmt.Sprintf("%x", h.Sum(nil))

	loginData := &misc.LoginData{ErrorDetails: ""}
	loginData.Error = ""
	loginData.Email = lastUsername
	loginData.SessionId, _ = utilities.GetSessionID(r)
	loginData.Token = token
	loginData.Valid = false
	if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
		authCookie.Options.Path = "/"
		authCookie.Values["lastloginattempt"], err = time.Now().UTC().MarshalText()
		authCookie.Values["token"] = token
		authCookie.Save(r, w)
	}
	sess, _ := utilities.GetSession(r)
	sess.Values["loginmsg"] = nil
	sess.Save(r, w)

	w.Header().Set("Content-type", "application/json")
	json_bytes, _ := json.Marshal(loginData)
	fmt.Fprintf(w, "%s\n", json_bytes)
	return
}

// Function to logout a logged in user
func handleLogout(w http.ResponseWriter, r *http.Request) {
	sess, _ := utilities.GetSession(r)
	sess.Values["loggedin"] = false
	sess.Values["loginmsg"] = "Successfully logged out"
	sess.Save(r, w)
	const _24K = (1 << 20) * 1024
	if err := r.ParseMultipartForm(_24K); nil != err {
		err = r.ParseForm()
		if err != nil {
			base.ErrorHandler(w, r, err, http.StatusInternalServerError)
			return
		}
	}
	email := r.FormValue("email")
	logoutData := misc.LogoutData{Email: email, Valid: false, LoggedOut: true}
	w.Header().Set("Content-type", "application/json")
	json_bytes, _ := json.Marshal(logoutData)
	fmt.Fprintf(w, "%s\n", json_bytes)
	return
}

// Function to handle login from a client.
// Several checks take place to ensure this client is genuine.
// For one, the session ID is checked. This should have been a hidden field value
// Next, a check is made for the cookiestore value in lastloginattempt.
// This is a time marshaled as text. If the user is reattempting before 3 seconds has ellasped
// reset the attempt to now and simply return a blank page. This user may be malicious
func handleLogin(w http.ResponseWriter, r *http.Request) {
	const _24K = (1 << 20) * 1024
	if err := r.ParseMultipartForm(_24K); nil != err {
		err = r.ParseForm()
		if err != nil {
			base.ErrorHandler(w, r, err, http.StatusInternalServerError)
			return
		}
	}
	var currToken string
	if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
		if (authCookie.Values["lastloginattempt"] != nil) && (authCookie.Values["lastloginattempt"] != "") {
			t := time.Now().UTC()
			t2 := time.Now().UTC()
			err := t2.UnmarshalText(authCookie.Values["lastloginattempt"].([]byte))
			if err != nil {
				base.ErrorHandler(w, r, err, http.StatusExpectationFailed)
				return
			}
			t2.Add(time.Second * 3)
			if t2.After(t) {
				base.ErrorHandler(w, r, errors.New("Could not process high threshold simultaneous request from same client. Wait then retry!"), http.StatusForbidden)
				return
			}
			crutime := time.Now().Unix()
			h := md5.New()
			io.WriteString(h, strconv.FormatInt(crutime, 10))
			token := fmt.Sprintf("%x", h.Sum(nil))
			authCookie.Values["lastloginattempt"], err = time.Now().UTC().MarshalText()
			currToken = string(authCookie.Values["token"].(string))
			authCookie.Values["token"] = token
			authCookie.Options.Path = "/"
			authCookie.Save(r, w)
			if userCookie, err := globals.WebCookiesStore.Get(r, "login-info"); err == nil {
				userCookie.Options.Path = "/"
				userCookie.Values["username"] = strings.ToLower(r.FormValue("username"))
				userCookie.Values["rememberme"] = true
				userCookie.Save(r, w)
			}
			if (currToken == "") || (r.FormValue("login-token") == "") || (r.FormValue("login-token") != currToken) {
				base.ErrorHandler(w, r, errors.New("No token has been set yet. Invalid request made."), http.StatusForbidden)
				return
			}
			currToken = token
		} else {
			base.ErrorHandler(w, r, errors.New("Invalid Cookie information. Cannot process requests."), http.StatusForbidden)
			return
		}

		// All is well with the first check. Server isn't being badgered
		sessionid := r.FormValue("session-id")
		username := strings.Trim(r.FormValue("username"), " ")
		username = strings.ToLower(username)
		password := r.FormValue("password")
		// Check if that session is valid..
		rsessid, _ := utilities.GetSessionID(r)
		if sessionid != rsessid {
			base.ErrorHandler(w, r, errors.New("Invalid Session information. Cannot process requests."), http.StatusForbidden)
			return
		}

		loginData := misc.LoginData{Error: "", Email: "", ErrorDetails: "", Token: "", SessionId: ""}
		loginErrors := ""

		aValidator := regexp.MustCompile(".+@.+\\..+")
		matched := aValidator.Match([]byte(username))
		if matched == false {
			loginErrors = loginErrors + "Please enter a valid email\n"
		}
		aValidator = regexp.MustCompile("^([a-zA-Z0-9@*# !]{4,15})$")
		matched = aValidator.Match([]byte(password))
		if matched == false {
			loginErrors = loginErrors + "Password must be between 4 and 15 characters\n"
		}

		loginData.Email = username
		loginData.Token = currToken
		loginData.SessionId = rsessid
		loginData.Valid = false

		if len(loginErrors) == 0 {
			// Here validate the user
			dbPassword := base32.StdEncoding.EncodeToString([]byte(password))
			dbColHandler := globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "users")
			pQuery := dbColHandler.Find(bson.M{"email": username, "passwordhash": dbPassword})
			iCount, err := pQuery.Count()
			if err != nil {
				base.ErrorHandler(w, r, err, http.StatusInternalServerError)
				return
			}
			if iCount == 0 {
				loginErrors = loginErrors + "Invalid email and or password\n"
			} else {
				// Login was successful
				// update the sessionid on the user table
				user := user.UserStruct{}
				err = pQuery.One(&user)
				if err != nil {
					base.ErrorHandler(w, r, err, http.StatusInternalServerError)
					return
				}
				err = dbColHandler.UpdateId(user.Id, bson.M{"$set": bson.M{"sessionid": rsessid}})
				if err != nil {
					base.ErrorHandler(w, r, err, http.StatusInternalServerError)
					return
				}
				err = logUserLogin(r, &user)
				if err != nil {
					base.ErrorHandler(w, r, err, http.StatusInternalServerError)
					return
				}
				sess, _ := utilities.GetSession(r)
				sess.Values["loggedin"] = true
				sess.Save(r, w)

				// We've set everything necessary
				loginData.Valid = true
				loginData.Profile = user
				w.Header().Set("Content-type", "application/json")
				json_bytes, _ := json.Marshal(loginData)
				fmt.Fprintf(w, "%s\n", json_bytes)
				return
			}
		}

		// If we are here, there must be errors during login
		loginData.Error = "Error Occurred During Login"
		loginData.ErrorDetails = loginErrors
		sess, _ := utilities.GetSession(r)
		if (sess.Values["loginmsg"] != nil) && (sess.Values["loginmsg"].(string) != "") {
			loginData.ErrorDetails = loginData.ErrorDetails + sess.Values["loginmsg"].(string)
		}
		sess.Values["loginmsg"] = nil
		sess.Save(r, w)

		w.Header().Set("Content-type", "application/json")
		json_bytes, _ := json.Marshal(loginData)
		fmt.Fprintf(w, "%s\n", json_bytes)
		return
	} else {
		base.ErrorHandler(w, r, errors.New("Invalid Cookie information. Cannot process requests."), http.StatusForbidden)
		return
	}
}

// Log information about a user login
func logUserLogin(r *http.Request, user *user.UserStruct) error {
	user.LastLogon = user.RecentLogon
	t := time.Now()
	tVal, err := t.MarshalText()
	if err != nil {
		return err
	}
	user.RecentLogon = string(tVal)
	dbColHandler := globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "users")
	err = dbColHandler.UpdateId(user.Id, bson.M{"$set": bson.M{"lastlogon": user.LastLogon, "recentlogon": user.RecentLogon}})
	return nil
}
