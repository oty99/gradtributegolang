package generic

import (
	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/oty99/gradtributegolang/models/generic"
	"bitbucket.org/oty99/gradtributegolang/models/misc"
	"bitbucket.org/oty99/gradtributegolang/models/user"
	"gopkg.in/mgo.v2/bson"
)

func GetModuleOutput(module string, loginData misc.LoginData) (*generic.ModuleStruct, error) {
	result := &generic.ModuleStruct{}
	result.Error = ""
	result.LoginData = loginData
	// fetch communications details
	dbColHandler := globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "moduledata")
	pQuery := dbColHandler.Find(bson.M{"module": module})
	iCount, err := pQuery.Count()
	if err != nil {
		return nil, err
	}
	if iCount == 0 {
		result.Error = "Failed to retrieve Communication Module Data"
		return result, err
	}
	CommsInfo := generic.ModuleInfo{}
	err = pQuery.One(&CommsInfo)
	if err != nil {
		return nil, err
	}
	result.Info = CommsInfo
	// Now, fetch users comms data
	dbColHandler = globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "users")
	pQuery = dbColHandler.Find(bson.M{"email": loginData.Email})
	iCount, err = pQuery.Count()
	if err != nil {
		return nil, err
	}
	if iCount == 0 {
		result.Error = "Failed to retrieve User Data"
		return result, err
	}
	user := user.UserStruct{}
	err = pQuery.One(&user)
	if err != nil {
		return nil, err
	}
	dbColHandler = globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "modulescore")
	pQuery = dbColHandler.Find(bson.M{"module": module, "userid": user.Id})
	iCount, err = pQuery.Count()
	if err != nil {
		return nil, err
	}
	if iCount == 0 {
		result.Error = "Failed to retrieve User Data"
		return result, err
	}
	userData := generic.ModuleUserScore{}
	err = pQuery.One(&userData)
	if err != nil {
		return nil, err
	}
	result.UserDetails = userData
	return result, err
}
