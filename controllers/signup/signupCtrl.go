package signup

import (
	"crypto/md5"
	"encoding/base32"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/middleware"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/utilities"
	"bitbucket.org/oty99/gradtributegolang/controllers/base"
	"bitbucket.org/oty99/gradtributegolang/models/misc"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"gopkg.in/mgo.v2/bson"
	"bitbucket.org/oty99/gradtributegolang/models/user"
)

// Initialize the routes that could come to this controller
// Generally, we expect that javascript jQuery posts would be used for
// submissions. However we also expect a get request for the login page
func Initialize(rRouter *mux.Router) {
	// The routes passed in here are built upon
	lChain := alice.New(middleware.GZIPHandler, middleware.SessionHandler)
	rRouter.Handle("/signup", lChain.ThenFunc(handleSignup)).Methods("POST")
}

// handleSignup performs the signup action if possible
func handleSignup(w http.ResponseWriter, r *http.Request) {
	const _24K = (1 << 20) * 1024
	if err := r.ParseMultipartForm(_24K); nil != err {
		err = r.ParseForm()
		if err != nil {
			base.ErrorHandler(w, r, err, http.StatusInternalServerError)
			return
		}
	}
	var currToken string
	if authCookie, err := globals.WebCookiesStore.Get(r, "user-auth"); err == nil {
		if (authCookie.Values["lastloginattempt"] != nil) && (authCookie.Values["lastloginattempt"] != "") {
			t := time.Now().UTC()
			t2 := time.Now().UTC()
			err := t2.UnmarshalText(authCookie.Values["lastloginattempt"].([]byte))
			if err != nil {
				base.ErrorHandler(w, r, err, http.StatusExpectationFailed)
				return
			}
			t2.Add(time.Second * 3)
			if t2.After(t) {
				base.ErrorHandler(w, r, errors.New("Could not process high threshold simultaneous request from same client. Wait then retry!"), http.StatusForbidden)
				return
			}
			crutime := time.Now().Unix()
			h := md5.New()
			io.WriteString(h, strconv.FormatInt(crutime, 10))
			token := fmt.Sprintf("%x", h.Sum(nil))
			authCookie.Values["lastloginattempt"], err = time.Now().UTC().MarshalText()
			currToken = string(authCookie.Values["token"].(string))
			authCookie.Values["token"] = token
			authCookie.Save(r, w)
			if (currToken == "") || (r.FormValue("login-token") == "") || (r.FormValue("login-token") != currToken) {
				base.ErrorHandler(w, r, errors.New("No token has been set yet. Invalid request made."), http.StatusForbidden)
				return
			}
			currToken = token
		} else {
			base.ErrorHandler(w, r, errors.New("Invalid Cookie information. Cannot process requests."), http.StatusForbidden)
			return
		}

		// All is well with the first check. Server isn't being badgered
		sessionid := r.FormValue("session-id")
		username := strings.Trim(r.FormValue("username"), " ")
		username = strings.ToLower(username)
		password := r.FormValue("password")
		firstname := strings.Trim(r.FormValue("firstname"), " ")
		lastname := strings.Trim(r.FormValue("lastname"), " ")
		usertype := ""
		res, eh := strconv.ParseBool(r.FormValue("student"))
		if eh != nil {
			base.ErrorHandler(w, r, errors.New("String to bool conversion failed"), http.StatusForbidden)
			return
		}
		if res {
			usertype = "student"
		} else {
			usertype = "professor"
		}
		// Check if that session is valid..
		rsessid, _ := utilities.GetSessionID(r)
		if sessionid != rsessid {
			base.ErrorHandler(w, r, errors.New("Invalid Session information. Cannot process requests."), http.StatusForbidden)
			return
		}

		signupData := misc.SignupData{Error: "", Email: "", ErrorDetails: "", Token: "", SessionId: ""}
		signupErrors := ""
		if len(firstname) < 3 {
			signupErrors = signupErrors + "First Name is required. Must be at least 3 characters\n"
		}
		if len(lastname) < 3 {
			signupErrors = signupErrors + "Last Name is required. Must be at least 3 characters\n"
		}
		aValidator := regexp.MustCompile(".+@.+\\..+")
		matched := aValidator.Match([]byte(username))
		if matched == false {
			signupErrors = signupErrors + "Please enter a valid email\n"
		}
		aValidator = regexp.MustCompile("^([a-zA-Z0-9@*# !]{4,15})$")
		matched = aValidator.Match([]byte(password))
		if matched == false {
			signupErrors = signupErrors + "Password must be between 4 and 15 characters\n"
		}
		signupData.Email = username
		signupData.Error = ""
		signupData.ErrorDetails = ""
		signupData.Message = ""
		signupData.SessionId = rsessid
		signupData.Token = currToken
		signupData.Valid = false

		if len(signupErrors) == 0 {
			dbPassword := base32.StdEncoding.EncodeToString([]byte(password))
			dbColHandler := globals.DBSession.DB(globals.DBSettings.DatabaseName).C(globals.DBSettings.CollectionPrefix + "users")
			pQuery := dbColHandler.Find(bson.M{"email": username})
			iCount, err := pQuery.Count()
			if err != nil {
				base.ErrorHandler(w, r, err, http.StatusInternalServerError)
				return
			}
			if iCount > 0 {
				signupErrors = signupErrors + "Email has already been taken. Try logging in.\n"
			} else {
				entry := &user.UserStruct{};
				entry.Email = username;
				entry.SessionId = rsessid;
				entry.Age = 0;
				entry.Avatar = "";
				entry.FirstName = firstname;
				entry.Gender = "";
				entry.LastLogon = "";
				entry.LastName = lastname;
				entry.MiddleName = "";
				entry.Phone.AreaCode = 0;
				entry.Phone.CountryCode = 0;
				entry.Phone.Number = 0;
				entry.Phone.Prefix = 0;
				entry.Position = "";
				entry.RecentLogon = "";
				entry.Role = "default";
				entry.UserType = usertype;
				entry.Password = dbPassword;

				err = dbColHandler.Insert(entry);
				if err != nil {
					base.ErrorHandler(w, r, err, http.StatusInternalServerError)
					return
				}
				signupData.Valid = true
				if usertype == "mentor" {
					signupData.Message = "Professor Monitoring is pending approval."
				}
				w.Header().Set("Content-type", "application/json")
				json_bytes, _ := json.Marshal(signupData)
				fmt.Fprintf(w, "%s\n", json_bytes)
				return
			}
		}
		// If we are here, there must be errors during login
		signupData.Error = "Error Occurred During Signup"
		signupData.ErrorDetails = signupErrors
		sess, _ := utilities.GetSession(r)
		if (sess.Values["loginmsg"] != nil) && (sess.Values["loginmsg"].(string) != "") {
			signupData.ErrorDetails = signupData.ErrorDetails + sess.Values["loginmsg"].(string)
		}
		sess.Values["loginmsg"] = nil
		sess.Save(r, w)

		w.Header().Set("Content-type", "application/json")
		json_bytes, _ := json.Marshal(signupData)
		fmt.Fprintf(w, "%s\n", json_bytes)
		return

	} else {
		base.ErrorHandler(w, r, errors.New("Invalid Cookie information. Cannot process requests."), http.StatusForbidden)
		return
	}
}
