package routes

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"github.com/shaoshing/train"

	"bitbucket.org/implasmicjafar/enjaeger/globals"
	"bitbucket.org/implasmicjafar/enjaeger/libraries/middleware"
	"bitbucket.org/oty99/gradtributegolang/controllers/login"
	"bitbucket.org/oty99/gradtributegolang/controllers/base"

	"bitbucket.org/oty99/gradtributegolang/controllers/signup"
	"bitbucket.org/oty99/gradtributegolang/controllers/profile"
	"bitbucket.org/oty99/gradtributegolang/controllers/communication"
)

//Initialize routes in general
func Initialize() *mux.Router {
	train.Config.AssetsPath = "assets"
	train.Config.BundleAssets = true
	if globals.IsProduction{
		train.Config.Mode = train.PRODUCTION_MODE
	}else{
		train.Config.Mode = train.DEVELOPMENT_MODE
		train.Config.Verbose = true
	}

	train.SetFileServer()

	//Compress Stuff, the default mux.
	http.DefaultServeMux.Handle(train.Config.AssetsUrl, middleware.GZIPHandler(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			train.ServeRequest(w, r)
		})))
	// Chain both session tracking and compressed output. ITs going to be on the root router
	publicRootChain := alice.New(middleware.GZIPHandler, middleware.SessionHandler)

	rRouter := mux.NewRouter().StrictSlash(true)
	rRouter.NotFoundHandler = publicRootChain.ThenFunc(base.Error404Handler)

	// Now we initialize the controllers

	login.Initialize(rRouter)
	signup.Initialize(rRouter)
	profile.Initialize(rRouter)
	communication.Initialize(rRouter)

	return rRouter
}


